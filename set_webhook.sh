#!/bin/sh -ex

URL=$(aws cloudformation describe-stacks \
      --stack-name $STACK_NAME \
      --region $REGION \
      --query 'Stacks[0].Outputs[?OutputKey==`SetWebhookApi`].OutputValue' \
      --output text)

echo "Set Webhook URL: $URL"
curl --silent --request POST $URL
