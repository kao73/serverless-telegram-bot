package com.simbirsoft.serverless.bot.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class AbstractCommand extends BotCommand {
    private static final Logger logger = LoggerFactory.getLogger(AbstractCommand.class);

    public AbstractCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    void execute(AbsSender sender, SendMessage message, User user) {
        try {
            sender.execute(message);
            logger.debug("message sent from {} to {}", getCommandIdentifier(), user.getId());
        } catch (TelegramApiException e) {
            logger.error("Unable to send message from {} to {}: {}", getCommandIdentifier(), user.getId(), e.getMessage());
        }
    }
}
