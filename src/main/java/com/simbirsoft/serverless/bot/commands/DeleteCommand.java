package com.simbirsoft.serverless.bot.commands;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import com.simbirsoft.serverless.bot.config.AppComponent;
import com.simbirsoft.serverless.bot.config.DaggerAppComponent;
import com.simbirsoft.serverless.bot.dao.NotesDao;
import com.simbirsoft.serverless.bot.exceptions.UnableToDeleteException;

public class DeleteCommand extends AbstractCommand {
    private static final Logger logger = LoggerFactory.getLogger(DeleteCommand.class);

    @Inject
    NotesDao notesDao;
    private final AppComponent configComponent;

    public DeleteCommand() {
        super("delete", "Delete notes");
        configComponent = DaggerAppComponent.builder().build();
        configComponent.inject(this);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        logger.debug("execute command {} for user {}", getCommandIdentifier(), user.getId());

        Long chatId = chat.getId();
        String message;
        try {
            notesDao.deleteNotes(chatId);
            message = "Notes deleted";
        } catch (UnableToDeleteException e) {
            message = "Please execute /start command first";
        }
        SendMessage answer = new SendMessage();
        answer.setChatId(chatId);
        answer.setText(message);
        execute(absSender, answer, user);
    }
}
