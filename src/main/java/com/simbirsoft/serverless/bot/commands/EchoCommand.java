package com.simbirsoft.serverless.bot.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public class EchoCommand extends AbstractCommand {
    private static final Logger logger = LoggerFactory.getLogger(EchoCommand.class);

    public EchoCommand() {
        super("echo", "Say something");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        logger.debug("execute command {} for user {}", getCommandIdentifier(), user.getId());

        String userName = chat.getUserName();
        if (userName == null || userName.isEmpty()) {
            userName = user.getFirstName() + " " + user.getLastName();
        }

        StringBuilder messageTextBuilder = new StringBuilder("Hey ").append(userName).append(", you said: ");
        if (arguments != null && arguments.length > 0) {
            messageTextBuilder.append(String.join(" ", arguments));
        }

        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setText(messageTextBuilder.toString());
        execute(absSender, answer, user);
    }
}
