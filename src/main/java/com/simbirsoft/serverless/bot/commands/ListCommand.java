package com.simbirsoft.serverless.bot.commands;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import com.simbirsoft.serverless.bot.config.AppComponent;
import com.simbirsoft.serverless.bot.config.DaggerAppComponent;
import com.simbirsoft.serverless.bot.dao.NotesDao;
import com.simbirsoft.serverless.bot.exceptions.NotesDoNotExistException;
import com.simbirsoft.serverless.bot.model.Notes;

import software.amazon.awssdk.utils.StringUtils;

public class ListCommand extends AbstractCommand {
    private static final Logger logger = LoggerFactory.getLogger(ListCommand.class);

    @Inject
    NotesDao notesDao;
    private final AppComponent configComponent;

    public ListCommand() {
        super("list", "List collected notes");
        configComponent = DaggerAppComponent.builder().build();
        configComponent.inject(this);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        logger.debug("execute command {} for user {}", getCommandIdentifier(), user.getId());

        Long chatId = chat.getId();
        String message;
        try {
            Notes notes = notesDao.getNotes(chatId);
            logger.debug("got notes: {}", notes);
            AtomicInteger counter = new AtomicInteger();
            message = notes.getNotes().stream()
                .map(n -> {
                    int count = counter.incrementAndGet();
                    return String.format("%d. %s\n", count, n);
                })
                .collect(Collectors.joining());
            logger.debug("generated list of messages: {}", message);
        } catch (NotesDoNotExistException e) {
            logger.error("unable to get notes for chat_id: {}", chatId);
            message = "Please execute /start command first";
        }
        if (StringUtils.isEmpty(message)) {
            message = "List is empty. Start typing";
        }
        SendMessage answer = new SendMessage();
        answer.setChatId(chatId);
        answer.setText(message);
        execute(absSender, answer, user);
    }
}
