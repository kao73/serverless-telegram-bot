package com.simbirsoft.serverless.bot.commands;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import com.google.common.collect.Lists;
import com.simbirsoft.serverless.bot.config.AppComponent;
import com.simbirsoft.serverless.bot.config.DaggerAppComponent;
import com.simbirsoft.serverless.bot.dao.NotesDao;
import com.simbirsoft.serverless.bot.exceptions.NotesDoNotExistException;
import com.simbirsoft.serverless.bot.model.Notes;

public class StartCommand extends AbstractCommand {
    private static final Logger logger = LoggerFactory.getLogger(StartCommand.class);

    private final AppComponent configComponent;

    @Inject
    NotesDao notesDao;

    public StartCommand() {
        super("start", "starts a new note list");

        configComponent = DaggerAppComponent.builder().build();
        configComponent.inject(this);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        logger.debug("execute command {} for user {}", getCommandIdentifier(), user.getId());

        String message;
        try {
            notesDao.getNotes(chat.getId());
            message = "List already started. Please /delete if not needed";
        } catch (NotesDoNotExistException e) {
            Notes newNotes = Notes.builder()
                .chatId(chat.getId())
                .notes(Lists.newArrayList())
                .build();
            notesDao.createNotes(newNotes);
            message = "A new list created";
        }

        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId());
        answer.setText(message);
        execute(absSender, answer, user);
    }
}
