package com.simbirsoft.serverless.bot.config;

import javax.inject.Singleton;

import com.simbirsoft.serverless.bot.commands.DeleteCommand;
import com.simbirsoft.serverless.bot.commands.ListCommand;
import com.simbirsoft.serverless.bot.commands.StartCommand;
import com.simbirsoft.serverless.bot.handler.WebhookHandler;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(WebhookHandler handler);
    void inject(StartCommand command);
    void inject(ListCommand listCommand);
    void inject(DeleteCommand deleteCommand);
}
