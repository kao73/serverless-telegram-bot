package com.simbirsoft.serverless.bot.config;

import java.net.URI;
import java.util.Optional;

import javax.inject.Named;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;
import com.simbirsoft.serverless.bot.dao.NotesDao;

import dagger.Module;
import dagger.Provides;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbClientBuilder;

@Module
public class AppModule {

    @Singleton
    @Provides
    ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Singleton
    @Provides
    @Named("tableName")
    String tableName() {
        return Optional.ofNullable(System.getenv("TABLE_NAME")).orElse("table_name");
    }

    @Singleton
    @Provides
    DynamoDbClient dynamoDb() {
        final String endpoint = System.getenv("ENDPOINT_OVERRIDE");

        DynamoDbClientBuilder builder = DynamoDbClient.builder();
        builder.httpClient(ApacheHttpClient.builder().build());
        if (endpoint != null && !endpoint.isEmpty()) {
            builder.endpointOverride(URI.create(endpoint));
        }

        return builder.build();
    }

    @Singleton
    @Provides
    public NotesDao gameDao(DynamoDbClient dynamoDb, @Named("tableName") String tableName) {
        return new NotesDao(dynamoDb, tableName);
    }
}
