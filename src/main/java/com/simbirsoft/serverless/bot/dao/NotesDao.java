package com.simbirsoft.serverless.bot.dao;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simbirsoft.serverless.bot.exceptions.CouldNotCreateNotesException;
import com.simbirsoft.serverless.bot.exceptions.NotesDoNotExistException;
import com.simbirsoft.serverless.bot.exceptions.TableDoesNotExistException;
import com.simbirsoft.serverless.bot.exceptions.UnableToDeleteException;
import com.simbirsoft.serverless.bot.exceptions.UnableToUpdateException;
import com.simbirsoft.serverless.bot.model.Notes;

import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemRequest;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemResponse;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.ReturnValue;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemResponse;

public class NotesDao {
    private static final Logger logger = LoggerFactory.getLogger(NotesDao.class);

    private static final String UPDATE_EXPRESSION
        = "SET notes = :notes ADD version :o";
    private final String tableName;
    private final DynamoDbClient dynamoDb;

    public NotesDao(final DynamoDbClient dynamoDb, final String tableName) {
        this.dynamoDb = dynamoDb;
        this.tableName = tableName;
    }

    public Notes createNotes(final Notes createNotesRequest) {
        if (createNotesRequest == null) {
            throw new IllegalArgumentException("createNotesRequest was null");
        }
        int tries = 0;
        while (tries < 10) {
            try {
                Map<String, AttributeValue> item = createNotesItem(createNotesRequest);
                dynamoDb.putItem(PutItemRequest.builder()
                    .tableName(tableName)
                    .item(item)
                    .conditionExpression("attribute_not_exists(chat_id)")
                    .build());
                return Notes.builder()
                    .chatId(Long.valueOf(item.get("chat_id").n()))
                    .notes(item.get("notes").ns())
                    .build();
            } catch (ConditionalCheckFailedException e) {
                tries++;
            } catch (ResourceNotFoundException e) {
                throw new TableDoesNotExistException(
                    "Notes table " + tableName + " does not exist");
            }
        }
        throw new CouldNotCreateNotesException("Unable to generate unique notes id after 10 tries");
    }

    public Notes getNotes(final Long chatId) {
        try {
            return Optional.ofNullable(
                dynamoDb.getItem(GetItemRequest.builder()
                    .tableName(tableName)
                    .key(Collections.singletonMap("chat_id", AttributeValue.builder().n(chatId.toString()).build()))
                    .build()))
                .map(GetItemResponse::item)
                .map(this::convert)
                .orElseThrow(() -> new NotesDoNotExistException("Notes " + chatId + " does not exist"));
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Notes table " + tableName + " does not exist");
        }
    }

    public Notes updateNotes(final Notes notes) {
        if (notes == null) {
            throw new IllegalArgumentException("Notes to update was null");
        }
        Long chatId = notes.getChatId();
        if (chatId == null) {
            throw new IllegalArgumentException("chatId was null");
        }
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
        Collection<AttributeValue> noteList = notes.getNotes().stream()
            .map(n -> AttributeValue.builder().s(n).build())
            .collect(Collectors.toList());
        expressionAttributeValues.put(":notes", AttributeValue.builder().l(noteList).build());

        expressionAttributeValues.put(":o", AttributeValue.builder().n("1").build());
        try {
            expressionAttributeValues.put(":v",
                AttributeValue.builder().n(notes.getVersion().toString()).build());
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("version was null");
        }
        final UpdateItemResponse result;
        try {
            result = dynamoDb.updateItem(UpdateItemRequest.builder()
                .tableName(tableName)
                .key(Collections.singletonMap("chat_id",
                    AttributeValue.builder().n(notes.getChatId().toString()).build()))
                .returnValues(ReturnValue.ALL_NEW)
                .updateExpression(UPDATE_EXPRESSION)
                .conditionExpression("attribute_exists(chat_id) AND version = :v")
                .expressionAttributeValues(expressionAttributeValues)
                .build());
        } catch (ConditionalCheckFailedException e) {
            throw new UnableToUpdateException(
                "Either the note did not exist or the provided version was not current");
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Notes table " + tableName
                + " does not exist and was deleted after reading the game");
        }
        return convert(result.attributes());
    }

    public Notes deleteNotes(final Long chatId) {
        final DeleteItemResponse result;
        try {
            return Optional.ofNullable(dynamoDb.deleteItem(DeleteItemRequest.builder()
                .tableName(tableName)
                .key(Collections.singletonMap("chat_id",
                    AttributeValue.builder().n(chatId.toString()).build()))
                .conditionExpression("attribute_exists(chat_id)")
                .returnValues(ReturnValue.ALL_OLD)
                .build()))
                .map(DeleteItemResponse::attributes)
                .map(this::convert)
                .orElseThrow(() -> new IllegalStateException(
                    "Condition passed but deleted item was null"));
        } catch (ConditionalCheckFailedException e) {
            throw new UnableToDeleteException(
                "A competing request changed the order while processing this request");
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Notes table " + tableName
                + " does not exist and was deleted after reading the order");
        }
    }

    private Notes convert(final Map<String, AttributeValue> item) {
        if (item == null || item.isEmpty()) {
            return null;
        }
        Notes.NotesBuilder builder = Notes.builder();
        logger.debug("list of attributes to convert: {}", item);

        try {
            builder.chatId(Long.valueOf(item.get("chat_id").n()));
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                "item did not have an chat_id attribute or it was not a Number");
        }

        try {
            List<String> notes = item.get("notes").l().stream().map(AttributeValue::s).collect(Collectors.toList());
            builder.notes(notes);
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                "item did not have an notes attribute or it was not a list of String");
        }

        try {
            builder.version(Long.valueOf(item.get("version").n()));
        } catch (NullPointerException | NumberFormatException e) {
            throw new IllegalStateException(
                "item did not have an version attribute or it was not a Number");
        }

        Notes result = builder.build();
        logger.debug("converted item: {}", result);
        return result;
    }

    private Map<String, AttributeValue> createNotesItem(final Notes notes) {
        Map<String, AttributeValue> item = new HashMap<>();
        item.put("chat_id", AttributeValue.builder().n(notes.getChatId().toString()).build());
        item.put("version", AttributeValue.builder().n("1").build());
        Collection<AttributeValue> noteList = notes.getNotes().stream()
            .map(n -> AttributeValue.builder().s(n).build())
            .collect(Collectors.toList());
        item.put("notes", AttributeValue.builder().l(noteList).build());
        return item;
    }
}
