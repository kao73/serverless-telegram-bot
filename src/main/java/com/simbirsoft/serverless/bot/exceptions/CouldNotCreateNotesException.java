package com.simbirsoft.serverless.bot.exceptions;

public class CouldNotCreateNotesException extends IllegalStateException {
    public CouldNotCreateNotesException(String message) {
        super(message);
    }
}
