package com.simbirsoft.serverless.bot.exceptions;

public class NotesDoNotExistException extends IllegalStateException {
    public NotesDoNotExistException(String message) {
        super(message);
    }
}
