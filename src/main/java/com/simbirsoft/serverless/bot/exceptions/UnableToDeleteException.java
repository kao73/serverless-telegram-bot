package com.simbirsoft.serverless.bot.exceptions;

public class UnableToDeleteException extends IllegalStateException {
    public UnableToDeleteException(String message) {
        super(message);
    }
}
