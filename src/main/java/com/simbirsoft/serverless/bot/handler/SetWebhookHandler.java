package com.simbirsoft.serverless.bot.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbirsoft.serverless.bot.model.GatewayResponse;

import static com.simbirsoft.serverless.bot.TelegramBotFactory.sender;
import static java.lang.System.getenv;

public class SetWebhookHandler implements IRequestStreamHandler {
    private static final Logger logger = LoggerFactory.getLogger(SetWebhookHandler.class);

    ObjectMapper objectMapper = new ObjectMapper();

    private static final TelegramWebhookBot SENDER = sender(getenv("TELEGRAM_ACCESS_TOKEN"), getenv("TELEGRAM_USERNAME"));

    @Override
    public void handleRequest(InputStream input, OutputStream output,
                              Context context) throws IOException {
        final JsonNode event;
        try {
            event = objectMapper.readTree(input);
        } catch (JsonMappingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, output, e.getMessage());
            return;
        }
        logger.debug("Event received: {}", event);

        // generate webhook url
        String url = String.format("https://%s/%s/webhook",
            event.get("headers").get("Host").asText(),
            event.get("requestContext").get("stage").asText()
        );
        logger.debug("SetWebhook URL: {}", url);

        // register webhook on TelegramApi
        try {
            SENDER.setWebhook(url, null);
        } catch (TelegramApiRequestException e) {
            logger.error("Unable to set webhook", e);
        }
        objectMapper.writeValue(output, new GatewayResponse<>("OK", APPLICATION_JSON, SC_OK));
    }
}
