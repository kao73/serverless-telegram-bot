package com.simbirsoft.serverless.bot.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.CommandRegistry;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.simbirsoft.serverless.bot.commands.DeleteCommand;
import com.simbirsoft.serverless.bot.commands.EchoCommand;
import com.simbirsoft.serverless.bot.commands.HelpCommand;
import com.simbirsoft.serverless.bot.commands.ListCommand;
import com.simbirsoft.serverless.bot.commands.StartCommand;
import com.simbirsoft.serverless.bot.config.AppComponent;
import com.simbirsoft.serverless.bot.config.DaggerAppComponent;
import com.simbirsoft.serverless.bot.dao.NotesDao;
import com.simbirsoft.serverless.bot.exceptions.NotesDoNotExistException;
import com.simbirsoft.serverless.bot.model.GatewayResponse;
import com.simbirsoft.serverless.bot.model.Notes;

import static com.simbirsoft.serverless.bot.TelegramBotFactory.sender;
import static java.lang.System.getenv;

public class WebhookHandler implements IRequestStreamHandler {
    private static final Logger logger = LoggerFactory.getLogger(WebhookHandler.class);

    private static final String TELEGRAM_ACCESS_TOKEN = getenv("TELEGRAM_ACCESS_TOKEN");
    private static final String TELEGRAM_USERNAME = getenv("TELEGRAM_USERNAME");

    private static final CommandRegistry commandRegistry = new CommandRegistry(false, TELEGRAM_USERNAME);
    private static final AbsSender SENDER = sender(TELEGRAM_ACCESS_TOKEN, TELEGRAM_USERNAME);

    @Inject
    ObjectMapper objectMapper;
    @Inject
    NotesDao notesDao;
    private final AppComponent configComponent;

    static {
        commandRegistry.register(new HelpCommand(commandRegistry));
        commandRegistry.register(new EchoCommand());
        commandRegistry.register(new StartCommand());
        commandRegistry.register(new ListCommand());
        commandRegistry.register(new DeleteCommand());
    }

    public WebhookHandler() {
        configComponent = DaggerAppComponent.builder().build();
        configComponent.inject(this);
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output,
                              Context context) throws IOException {
        final Update update;
        try {
            // read Lambda event
            final JsonNode event = objectMapper.readTree(input);
            logger.debug("Event received: {}", event);

            // read Telegram API update
            update = objectMapper.readValue(event.get("body").asText(), Update.class);
            logger.debug("TelegramApi update received: {}", update);

            // handle the message
            handleUpdate(update);
        } catch (JsonMappingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, output, e.getMessage());
            return;
        } catch (Exception e) {
            logger.error("Unable to perform update", e);
        }
        // send OK response to make sure TelegramApi will not call it infinitely
        objectMapper.writeValue(output, new GatewayResponse<>("OK", APPLICATION_JSON, SC_OK));
    }

    private void handleUpdate(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            if (message.isCommand()) {
                if (!commandRegistry.executeCommand(SENDER, message)) {
                    //we have received a not registered command, handle it as invalid
                    processInvalidCommandUpdate(update);
                }
                return;
            }
        }
        processNonCommandUpdate(update);
    }

    protected void processInvalidCommandUpdate(Update update) {
        processNonCommandUpdate(update);
    }

    private void processNonCommandUpdate(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();

            if (message.hasText()) {

                Long chatId = update.getMessage().getChatId();
                Notes notes;
                try {
                    notes = notesDao.getNotes(chatId);
                } catch (NotesDoNotExistException e) {
                    Notes newNotes = Notes.builder()
                        .chatId(chatId)
                        .notes(Lists.newArrayList())
                        .build();
                    notes = notesDao.createNotes(newNotes);
                }
                notes.getNotes().add(message.getText());
                notesDao.updateNotes(notes);

                SendMessage echoMessage = new SendMessage();
                echoMessage.setChatId(chatId);
                echoMessage.setText("Got it");

                try {
                    SENDER.execute(echoMessage);
                } catch (TelegramApiException e) {
                    logger.error("Unable to send message", e);
                }
            }
        }
    }
}
