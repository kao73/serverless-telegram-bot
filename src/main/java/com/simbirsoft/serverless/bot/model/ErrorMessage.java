package com.simbirsoft.serverless.bot.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@JsonAutoDetect
@AllArgsConstructor
public class ErrorMessage {
    private final String message;
    private final int statusCode;
}
